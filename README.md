# Simple docker containers for developers

This is a collection of simple docker-compose YAML for common services in developer environment.

<!-- TOC -->
* [Simple docker containers for developers](#simple-docker-containers-for-developers)
  * [Disclaimer](#disclaimer)
  * [Permissions](#permissions)
  * [Tools](#tools)
    * [lazydocker](#lazydocker)
  * [Container configuration](#container-configuration)
  * [Data storage](#data-storage)
  * [Common utility scripts](#common-utility-scripts)
    * [start.sh](#startsh)
    * [stop.sh](#stopsh)
    * [purge_data.sh](#purge_datash)
  * [Available containers](#available-containers)
    * [Elasticsearch](#elasticsearch)
    * [MariaDB](#mariadb)
    * [MySQL](#mysql)
    * [OpenSearch](#opensearch)
    * [RabbitMQ](#rabbitmq)
    * [Redis](#redis)
    * [Soketi](#soketi)
<!-- TOC -->

**DO NOT USE THESE CONTAINER DEFINITIONS IN PRODUCTION ENVIRONMENT**

These YAMLs are parametrized to make simpler the selection of different images and versions for the services.
With these container definitions you will be able to run multiple occurences of the same service with different
versions. As an example, MySQL and MariaDB can run without conflicts, just need to configure different host port.

The idea came from the need of having MySQL and MariaDB available for different projects. Such _atomic_ service
container allows keep running only needed ones. For my development tasks i have non need to access the containers'
environment since they ara used as a standalone server, just responding on localhost address.

For manual database access thera are no need to install a plethora of packages as Apache, Php, PhpMyAdmin since you
can access using common tools like MySQL Workbench or [DBEaver](https://dbeaver.io/) directly from your workstation.

## Disclaimer

I haven't used Windows in over ten years, so don't ask me for support for that operating system. I develop applications
for the Linux web environment, on a Linux workstation and I don't want to mess with an environment that I don't like and
that doesn't benefit me at all.

## Permissions

 You may wanto to add your user to the docker group in order to manage containers withous _sudo_

```bash
$ sudo usermod -a -G docker <username>
```
## Tools

### lazydocker

I found a simple tool that is very useful for container management, at least for my needs:
[lazydoker](https://github.com/jesseduffield/lazydocker). If you are not a docker-guru and If you don't have any special
needs, the features it offers are sufficient.

![lazydocker](docs/images/lazydocker.png)

## Container configuration

Each container may be customized to allow running multiple instances with different parameters such as IP port and data
directories in order to avoid conflicts. Just edit the .env file, for most of them jou just need to set proper
image name and version.

## Data storage

Containers data will be stored in the **_data_** directory, so no garbage will grow in the container definition
directories. Each service will have versione specific data structure to allow cuncurrent running of different versions without risk
of data corruption due to version incompatibility.

Example of data structure with different services and versions

![data structure example](docs/images/data-structure-example.png)

## Common utility scripts

Each container definition has some minimal shell script to simplify the management. They must be executed in the
specific directory.

### start.sh

Starts the container. For some containers provides to create the data directories in order to avoid permission errors
due to the ownership (root) when created by composer.

### stop.sh

Stops the container

### purge_data.sh

Deletes the data of the container for the current versione set in .env. Be careful when purging data and ensure .env
is set to the version you want to rwmove.

## Available containers

### Elasticsearch

Search engine

### MariaDB

Popular DB Server forked from MySQL

### MySQL

Popular DB Server

### OpenSearch

Search engine replacement of Elasticsearch

### RabbitMQ

Popular Message Queue service

### Redis

Cache server

### Soketi

Open Source alternative to Pusher, used by some Laravel applications


