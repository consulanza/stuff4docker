#!/bin/bash
source .env
#
function deleteDir {
  sudo chmod +w "$1"
  sudo rm -rf "$1"
}
#
deleteDir "${COMPOSE_PROJECT_ROOT}/../data/var/run${COMPOSE_PROJECT_NAME}_${STACK_VERSION}"
deleteDir "${COMPOSE_PROJECT_ROOT}/../data/var/log/${COMPOSE_PROJECT_NAME}_${STACK_VERSION}"

