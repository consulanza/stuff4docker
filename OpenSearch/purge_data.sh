#!/bin/bash
source .env
#
function deleteDir {
# data
  sudo chmod +w "${COMPOSE_PROJECT_ROOT}/../data/usr/share/${1}_${STACK_VERSION}"
  sudo rm -rf "${COMPOSE_PROJECT_ROOT}/../data/usr/share/${1}_${STACK_VERSION}"
# log
  sudo chmod +w "${COMPOSE_PROJECT_ROOT}/../data/var/log/${1}_${STACK_VERSION}"
  sudo rm -rf "${COMPOSE_PROJECT_ROOT}/../data/var/log/${1}_${STACK_VERSION}"
}
#
deleteDir ${COMPOSE_PROJECT_NAME}
#
