#!/bin/bash
#
source .env
#
# make data diretories if missing to avoid creation as root-owned and prevent persmission issues
#

function makeDir {
mkdir -p  "${COMPOSE_PROJECT_ROOT}/../data/usr/share/${1}_${STACK_VERSION}/data_1"
mkdir -p  "${COMPOSE_PROJECT_ROOT}/../data/usr/share/${1}_${STACK_VERSION}/data_2"
mkdir -p  "${COMPOSE_PROJECT_ROOT}/../data/var/log/${1}_${STACK_VERSION}"

}
makeDir ${COMPOSE_PROJECT_NAME}

#
dc=`which docker-compose`
$dc up -d
