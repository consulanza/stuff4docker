#!/bin/bash
#
source .env
#
# make data diretories if missing to avoid creation as root-owned and prevent persmission issues
#

function makeDir {
  mkdir -p  "${COMPOSE_PROJECT_ROOT}/../data/var/lib/${1}_${STACK_VERSION}/data"
  mkdir -p  "${COMPOSE_PROJECT_ROOT}/../data/var/log/${1}_${STACK_VERSION}"
  mkdir -p  "${COMPOSE_PROJECT_ROOT}/../data/etc/${1}_${STACK_VERSION}"
}
makeDir ${COMPOSE_PROJECT_NAME}

#
compose=`which docker-compose`
docker=`which docker`

$compose up -d
$docker ps -af "name=${COMPOSE_PROJECT_NAME}"
echo -e "Container started\n"
