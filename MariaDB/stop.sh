#!/bin/bash
#
source .env
#
docker=`which docker`
#
container_id=`$docker ps --no-trunc -aqf "name=${COMPOSE_PROJECT_NAME}"`
container_line=`$docker ps -af "name=${COMPOSE_PROJECT_NAME}"`

echo -e "Stopping container:\n{$container_line}"
$docker stop $container_id
echo -e "Container stopped\n"


